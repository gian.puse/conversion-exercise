import React from "react";
import ReactDOM from "react-dom";
import img1 from "./images/oqulo-logo.png";
import style from "./style.css";
import img2 from "./images/screen.png";
import img3 from "./images/Object31.png";
import graphic_bg from "./images/graphic-bg.png";
import background from "./images/coolbg2.png";
import iphone7 from "./images/iPhone-7.png";
import navbar from "./images/nav-bar.png";
import statistics from "./images/statistics.png";
import statistics2 from "./images/statistics2.png";
import background_small from "./images/background-copy.png";
import cta_text from "./images/cta_text.png";

const page = (
  <>
    <html>
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>OQULO</title>
        <link rel="stylesheet" href="style.css" />
      </head>
      <body style={{ backgroundImage: `url(${background})` }}>
        <div class="container">
          <div class="navbar">
            <img src={img1} alt="" className="oqulo-logo" class="logo" />
            <nav>
              <ul id="menuList">
                <li class="navList">
                  <a class="navList" href="">
                    DISCOVER OQULO
                  </a>
                </li>
                <li class="navList">
                  <a class="navList" href="">
                    FEATURES
                  </a>
                </li>
                <li class="navList">
                  <a class="navList" href="">
                    CONTACT
                  </a>
                </li>
              </ul>
            </nav>
            <img
              src={navbar}
              alt=""
              className="menu-icon"
              onclick="togglemenu()"
            />
          </div>

          <div class="row">
            <img src={iphone7} alt="" className="iphone" />

            <div class="col-1">
              <h2>
                The Only Platfrom You'll Need to <br />
                Run Smart Coworking Spaces & <br />
                Serviced Offices
              </h2>
              <br />
              <p>
                Oqulo is built to sell, manage and grow your commercial real
                estate business <br />
                Collect payments, manage clients and run reports using our
                booking app <br />
                Engage members using our community messaging feature
              </p>
              <br />
              <p>Be the first to take Oqulo for a test drive.</p>
              <br />
              <input
                className="emailInput"
                type="email"
                name="yourEmail"
                placeholder="Email address"
              />
              <button type="button">
                NOTIFY ME
                <img src="images/arrow.png" />
              </button>

              <p>
                <br />
                No spam, that's a promise.
              </p>
            </div>
          </div>
        </div>

        <div class="container2">
          <div class="navbar">
            <nav></nav>
          </div>

          <div class="row">
            <div class="col-1-2">
              <h2>
                Tried and Tested Space
                <br />
                Management Software
                <br />
              </h2>
              <br />
              <p>
                Oqulo is a homegrown app that's been tested by real-life <br />
                businesses. Whether you operate on a single building or in{" "}
                <br />
                multiple locations, Oqulo is designed to make your space <br />
                leasing operations hassle-free.
                <br />
              </p>
              <p>
                Your clients will have a smooth booking & online payment
                <br />
                experience, and your concierge staff will be able to view
                <br />
                occupancy stats and generate reports at a click of a button.
                <br />
              </p>
            </div>
            <img src={img3} alt="" className="object31" />
          </div>
          <br />
          <br />
          <br />
          <br />
        </div>

        <div class="container3">
          <div class="navbar">
            <nav></nav>
          </div>
          <div class="columnTitle">
            <h2 class="center">Oqulo Features at a Glance</h2>
            <p class="center">
              Power functionalities that changes the way you do business
            </p>
          </div>
          <div class="row">
            <div class="col-1-2" className="c3-left-col">
              <div class="feature1">
                <h3>Powerful space management</h3>
                <p>
                  Manage meeting room and desk
                  <br />
                  bookings, create events, sell tickets,
                  <br />
                  schedule private office showings,
                  <br />
                  automate invoicing and connect with <br />
                  members --- all in one central <br />
                  dashboard.
                  <br />
                </p>
                <br />
              </div>
              <h3>Painless Integration</h3>
              <p>
                No matter your website is built <br />
                on, Oqulo is easy to setup and <br />
                integrate with CRM and payment <br />
                gateways. Go live in a matter of days
              </p>
              <p>
                Your clients will have a smooth booking & online payment
                <br />
                experience, and your concierge staff will be able to view
                <br />
                occupancy stats and generate reports at a click of a button.
                <br />
              </p>
            </div>
            <img src={img2} alt="" className="screen" />
            <div class="col-1-2">
              <h3>User-Friendly Interface</h3>
              <p>
                Clients will find it easy to book and pay <br />
                for their space, thanks to Oqulo's easy <br />
                navigation and pixel-perfect design.
                <br />
                Keep members up to date with Oqulo's
                <br />
                community board and help desk features.
                <br />
              </p>
              <br />
              <h3>Secure Data & White Label Branding</h3>
              <p>
                Get peace of mind in knowing that
                <br />
                client information and sales date are
                <br />
                stored in a secure server. Our white
                <br />
                label service allows you to market this <br />
                platform as your own.
                <br />
              </p>
            </div>
          </div>
        </div>

        <div className="container4">
          <br />
          <br />
          <br />
          <h1>Stats delivered beautifully</h1>
          <p>
            View sales charts, booking ratio and user behaviour using Oqulo's
            visual reporting feature.
          </p>
          <img src={statistics} alt="" className="statistics" />
          <img src={statistics2} alt="" className="statistics2" />
        </div>

        <div className="container5">
          <img src={cta_text} alt="" className="cta_text" />
          <img src={background_small} alt="" className="background_small" />
        </div>

        <div className="container6">
          <br />
          <br />
          <br />
          <br />
          <img src={img1} alt="" className="logo2" />
          <br />
          <br />
          <br />
          <div className="social-links">
            <ul className="navbar2">
              <li className="navbar2">DISCOVER OQULO</li>
              <li className="navbar2">FEATURES</li>
              <li className="navbar2">CONTACTS</li>
            </ul>
            <br />
            <br />
          </div>
          <p className="copyright">
            Copyright © Oqulo 2018. All Rights Reserved.
          </p>
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
        </div>

        {/* <script>
      var menuList = document.getElementById("menuList");

      menuList.style.maxHeight = "0px";

      function togglemenu() {
        if (menuList.style.maxHeight == "0px") {
          menuList.style.maxHeight = "130px";
        } else {
          menuList.style.maxHeight = "0px";
        }
      }
    </script> */}
      </body>
    </html>
  </>
);

ReactDOM.render(page, document.getElementById("root"));
